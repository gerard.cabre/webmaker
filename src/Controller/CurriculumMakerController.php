<?php

namespace App\Controller;

use mysqli;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CurriculumMakerController extends AbstractController
{
    /**
     * @Route("/curriculum/{nomDomini}")
     */
    public function index(Request $request): Response
    {
        $domini = $request->attributes->get('nomDomini');
        $arrayCamps = selectUser($domini);
        if (!$arrayCamps){
            return $this->render('curriculum_maker/error404.html.twig');
        }

        if ($arrayCamps["tema"] == "funny"){
            return  $this->render('curriculum_maker/indexfunny.html.twig', [
                'nom' => $arrayCamps["nom"],
                'cognom' => $arrayCamps["cognom"],
                "edat" =>$arrayCamps["edat"],
                "descripcio" => $arrayCamps["descripcio"],
                "tema" => $arrayCamps["tema"],
                "direccio" =>$arrayCamps["direccio"],
                "email" => $arrayCamps["email"],
                "twitter" => $arrayCamps["twitter"],
                "linkedin" => $arrayCamps["linkedin"],
                "infojobs" => $arrayCamps["infojobs"],
                "web" => $arrayCamps["web"],
                "idiomes" => $arrayCamps["idiomes"],
                "telefon" => $arrayCamps["telefon"],
                "nomcurs1" => $arrayCamps["nomcurs1"],
                "duradacurs1" => $arrayCamps["duradacurs1"],
                "nomcurs2" => $arrayCamps["nomcurs2"],
                "duradacurs2" => $arrayCamps["duradacurs2"],
                "curs1" => $arrayCamps["curs1"],
                "curs2" => $arrayCamps["curs2"],
                "curs3" => $arrayCamps["curs3"],
                "curs4" => $arrayCamps["curs4"],
                "nomempresa1" => $arrayCamps["nomempresa1"],
                "tempsempresa1" => $arrayCamps["tempsempresa1"],
                "nomempresa2" => $arrayCamps["nomempresa2"],
                "tempsempresa2" => $arrayCamps["tempsempresa2"],
                "nomempresa3" => $arrayCamps["nomempresa3"],
                "tempsempresa3" => $arrayCamps["tempsempresa3"]
            ]);
        }
        return $this->render('curriculum_maker/index.html.twig', [
            'nom' => $arrayCamps["nom"],
            'cognom' => $arrayCamps["cognom"],
            "edat" =>$arrayCamps["edat"],
            "descripcio" => $arrayCamps["descripcio"],
            "tema" => $arrayCamps["tema"],
            "direccio" =>$arrayCamps["direccio"],
            "email" => $arrayCamps["email"],
            "twitter" => $arrayCamps["twitter"],
            "linkedin" => $arrayCamps["linkedin"],
            "infojobs" => $arrayCamps["infojobs"],
            "web" => $arrayCamps["web"],
            "idiomes" => $arrayCamps["idiomes"],
            "telefon" => $arrayCamps["telefon"],
            "nomcurs1" => $arrayCamps["nomcurs1"],
            "duradacurs1" => $arrayCamps["duradacurs1"],
            "nomcurs2" => $arrayCamps["nomcurs2"],
            "duradacurs2" => $arrayCamps["duradacurs2"],
            "curs1" => $arrayCamps["curs1"],
            "curs2" => $arrayCamps["curs2"],
            "curs3" => $arrayCamps["curs3"],
            "curs4" => $arrayCamps["curs4"],
            "nomempresa1" => $arrayCamps["nomempresa1"],
            "tempsempresa1" => $arrayCamps["tempsempresa1"],
            "nomempresa2" => $arrayCamps["nomempresa2"],
            "tempsempresa2" => $arrayCamps["tempsempresa2"],
            "nomempresa3" => $arrayCamps["nomempresa3"],
            "tempsempresa3" => $arrayCamps["tempsempresa3"]
        ]);
    }

    /**
     *
     * @Route("/insertcurriculum")
     */
    public function insertBd(Request $request): Response
    {
        $servername = "localhost";
        $username = "root";
        $password = "";

        $conn = mysqli_connect($servername, $username, $password);
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }

        $nom = $request->request->get("nom");
        $cognom = $request->request->get("cognom");
        $edat = $request->request->get("edat");
        $domini = $request->request->get("domini");
        $foto = $request->request->get("foto");
        $descripcio = $request->request->get("descripcio");
        $tema = $request->request->get("tema");
        $direccio = $request->request->get("direccio");
        $email = $request->request->get("email");
        $twitter = $request->request->get("twitter");
        $linkedin = $request->request->get("linkedin");
        $infojobs = $request->request->get("infojobs");
        $web = $request->request->get("web");
        $idiomes = $request->request->get("idiomes");
        $telefon = $request->request->get("telefon");
        $nomcurs1 = $request->request->get("nomcurs1");
        $duradacurs1 = $request->request->get("duradacurs1");
        $nomcurs2 = $request->request->get("nomcurs2");
        $duradacurs2 = $request->request->get("duradacurs2");
        $curs1 = $request->request->get("curs1");
        $curs2 = $request->request->get("curs2");
        $curs3 = $request->request->get("curs3");
        $curs4 = $request->request->get("curs4");
        $nomempresa1 = $request->request->get("nomempresa1");
        $tempsempresa1 = $request->request->get("tempsempresa1");
        $nomempresa2 = $request->request->get("nomempresa2");
        $tempsempresa2 = $request->request->get("tempsempresa2");
        $nomempresa3 = $request->request->get("nomempresa3");
        $tempsempresa3 = $request->request->get("tempsempresa3");

        $sql = "INSERT INTO `webmaker`.`curriculum` (nom,cognom,edat,domini,imatge,descripcio,tema,direccio,email,twitter,linkedin,infojobs,web,idiomes,telefon,nomcurs1,duradacurs1,nomcurs2,duradacurs2,curs1,curs2,curs3,curs4,nomempresa1,tempsempresa1,nomempresa2,tempsempresa2,nomempresa3,tempsempresa3) VALUES ('".$nom."','".$cognom."','".$edat."','".$domini."','".$foto."','".$descripcio."','".$tema."','".$direccio."','".$email."','".$twitter."','".$linkedin."','".$infojobs."','".$web."','".$idiomes."','".$telefon."','".$nomcurs1."','".$duradacurs1."','".$nomcurs2."','".$duradacurs2."','".$curs1."','".$curs2."','".$curs3."','".$curs4."','".$nomempresa1."','".$tempsempresa1."','".$nomempresa2."','".$tempsempresa2."','".$nomempresa3."','".$tempsempresa3."')";

        $conn->query($sql);

        mysqli_close($conn);

        return $this->render('test/index.html.twig', [
            'domini' => $domini,
        ]);
    }

}


function selectUser($domini)
{
    $servername = "localhost";
    $username = "root";
    $password = "";

    $conn = mysqli_connect($servername, $username, $password);
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $sql = "SELECT * FROM `webmaker`.`curriculum` WHERE domini = '".$domini ."'";

    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $arrayCampsBd = [
                "nom" => $row["nom"],
                "cognom" =>$row["cognom"],
                "edat" =>$row["edat"],
                "descripcio" => $row["descripcio"],
                "tema" => $row["tema"],
                "direccio" =>$row["direccio"],
                "email" => $row["email"],
                "twitter" => $row["twitter"],
                "linkedin" => $row["linkedin"],
                "infojobs" => $row["infojobs"],
                "web" => $row["web"],
                "idiomes" => $row["idiomes"],
                "telefon" => $row["telefon"],
                "nomcurs1" => $row["nomcurs1"],
                "duradacurs1" => $row["duradacurs1"],
                "nomcurs2" => $row["nomcurs2"],
                "duradacurs2" => $row["duradacurs2"],
                "curs1" => $row["curs1"],
                "curs2" => $row["curs2"],
                "curs3" => $row["curs3"],
                "curs4" => $row["curs4"],
                "nomempresa1" => $row["nomempresa1"],
                "tempsempresa1" => $row["tempsempresa1"],
                "nomempresa2" => $row["nomempresa2"],
                "tempsempresa2" => $row["tempsempresa2"],
                "nomempresa3" => $row["nomempresa3"],
                "tempsempresa3" => $row["tempsempresa3"]
            ];

            return $arrayCampsBd;
        }
    }

    mysqli_close($conn);
}


// symfony server:start