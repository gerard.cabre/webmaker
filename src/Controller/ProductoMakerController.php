<?php

namespace App\Controller;

use mysqli;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductoMakerController extends AbstractController
{


    public function index(Request $request): Response
    {
        $nomProducte = $request->attributes->get('nomProducte');
        $arrayCamps = selectProduct($nomProducte);
        if (!$arrayCamps){
            return $this->render('curriculum_maker/error404.html.twig');
        }
        if ($arrayCamps["tema"] == "claro"){
            return $this->render('product_maker/indexclaro.html.twig', [
                'nom' => $arrayCamps["nom"],
                "preu" =>$arrayCamps["preu"],
                "descripcio" =>$arrayCamps["descripcio"],
                "foto" =>$arrayCamps["foto"],
                "carac1" => $arrayCamps["carac1"],
                "desc1" => $arrayCamps["desc1"],
                "carac2" => $arrayCamps["carac2"],
                "desc2" => $arrayCamps["desc2"],
                "carac3"=> $arrayCamps["carac3"],
                "desc3" => $arrayCamps["desc3"],
                "carac4" => $arrayCamps["carac4"],
                "desc4" => $arrayCamps["desc4"],
                "tema" => $arrayCamps["tema"]
            ]);
        }
        return $this->render('product_maker/index.html.twig', [
            'nom' => $arrayCamps["nom"],
            "preu" =>$arrayCamps["preu"],
            "descripcio" =>$arrayCamps["descripcio"],
            "foto" =>$arrayCamps["foto"],
            "carac1" => $arrayCamps["carac1"],
            "desc1" => $arrayCamps["desc1"],
            "carac2" => $arrayCamps["carac2"],
            "desc2" => $arrayCamps["desc2"],
            "carac3"=> $arrayCamps["carac3"],
            "desc3" => $arrayCamps["desc3"],
            "carac4" => $arrayCamps["carac4"],
            "desc4" => $arrayCamps["desc4"],
            "tema" => $arrayCamps["tema"]
        ]);
    }


    public function insertBd(Request $request): Response
    {
        $servername = "localhost";
        $username = "root";
        $password = "";

        $conn = mysqli_connect($servername, $username, $password);
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }

        $nom = $request->request->get("nom");
        $preu = $request->request->get("preu");
        $domini = $request->request->get("domini");
        $tema = $request->request->get("tema");
        $desc = $request->request->get("desc");
        $foto = $request->request->get("foto");
        $carac1 = $request->request->get("carac1");
        $desc1 = $request->request->get("desc1");
        $carac2 = $request->request->get("carac2");
        $desc2 = $request->request->get("desc2");
        $carac3 = $request->request->get("carac3");
        $desc3 = $request->request->get("desc3");
        $carac4 = $request->request->get("carac4");
        $desc4 = $request->request->get("desc4");

        $sql = "INSERT INTO `webmaker`.`product` (nom,preu,descripcio,foto,carac1,desc1,carac2,desc2,carac3,desc3,carac4,desc4, domini,tema) VALUES ('".$nom."','".$preu."','".$desc."','".$foto."','".$carac1."','".$desc1."','".$carac2."','".$desc2."','".$carac3."','".$desc3."','".$carac4."','".$desc4."','".$domini."','".$tema."')";

        $conn->query($sql);

        mysqli_close($conn);

        return $this->render('test/index2.html.twig', [
            'domini' => $request->request->get("domini"),
        ]);
    }

}


function selectProduct($nomProducte)
{
    $servername = "localhost";
    $username = "root";
    $password = "";

    $conn = mysqli_connect($servername, $username, $password);
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $sql = "SELECT * FROM `webmaker`.`product` WHERE domini = '".$nomProducte ."'";

    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $arrayCampsBd = [
                "nom" => $row["nom"],
                "preu" =>$row["preu"],
                "descripcio" =>$row["descripcio"],
                "foto" =>$row["foto"],
                "carac1" => $row["carac1"],
                "desc1" => $row["desc1"],
                "carac2" => $row["carac2"],
                "desc2" => $row["desc2"],
                "carac3"=> $row["carac3"],
                "desc3" => $row["desc3"],
                "carac4" => $row["carac4"],
                "desc4" => $row["desc4"],
                "tema" => $row["tema"]
            ];

            return $arrayCampsBd;
        }
    }

    mysqli_close($conn);
}


// symfony server:start