<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductoFormController extends AbstractController
{

    public function index(): Response
    {
        return $this->render('product_form/index.html.twig');
    }
}